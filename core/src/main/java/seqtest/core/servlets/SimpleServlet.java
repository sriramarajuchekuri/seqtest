/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package seqtest.core.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Session;
import javax.mail.MessagingException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
 
import com.day.cq.commons.mail.MailTemplate ;

import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

import seqtest.core.models.service.SequenceTestService;
import seqtest.core.models.service.SequenceTestServiceImpl;



/**
 * Servlet that writes some sample content into the response. It is mounted for
 * all resources of a specific Sling resource type. The
 * {@link SlingSafeMethodsServlet} shall be used for HTTP methods that are
 * idempotent. For write operations use the {@link SlingAllMethodsServlet}.
 */
@Component(service=Servlet.class,
           property={
                   Constants.SERVICE_DESCRIPTION + "=Simple Demo Servlet",
                   "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                   "sling.servlet.resourceTypes="+ "seqtest/components/structure/page",
                   "sling.servlet.extensions=" + "html"
           })
public class SimpleServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUid = 1L;
    
    private static final Logger log = LoggerFactory.getLogger(SimpleServlet.class);

    @Reference
    SequenceTestService service;
    
   
    String[] orderedChildNodes = null;
    
    @Override
    protected void doGet(final SlingHttpServletRequest request,
            final SlingHttpServletResponse response) throws ServletException, IOException {
    	
    	
    	
    	try
        {
    		orderedChildNodes = service.ExecuteSortingOrder();
    		
    		   		
    		log.info("---> THIS IS THE GET METHOD OF seqtest/components/structure/page");
            PrintWriter out = response.getWriter();
            sendEmail(request,orderedChildNodes);
            out.println("<html><body>");
            if(orderedChildNodes[0].equals("FAILED")){
            	
	            out.println("<h1>");
	            out.println("Node Sorting Failed!");
	            out.println("</br>");
	            out.println("</br>");
	            out.println("</br>");
	            out.println(orderedChildNodes[1]);
	            out.println("</br>");
	            out.println("</br>");
	            out.println("Failed Sort email notification sent.");
	            
	            
            }else{
	            
	            out.println("<h1>");
	            out.println("Node Sorting Completed!. Resequenced in following order.");
	            out.println("</br>");
	            
	            for(int i=0; i<=orderedChildNodes.length-1;i++){
	            	out.println(orderedChildNodes[i]);
	            	out.println("</br>");
	            }
	            out.println("</br>");
	            out.println("Successful Sort email notification sent.");
	           
            }
            
            
            
           
            out.println("</h1>");
            out.println("</html></body>");
        }
        catch(Exception e)
        {
        	log.info(e.getMessage(),e);
        }
    }
    

    @Reference
    private MessageGatewayService messageGatewayService;
    
    public void sendEmail(SlingHttpServletRequest request,String[] orderedChildNodes){
		 
		 String emailMessage = null;
		 if(orderedChildNodes[0].equals("FAILED")){
			 emailMessage = "Nodes sorting has failed. ";
		 }else{
			 emailMessage = "Nodes sorted in the order : ";
			 for(int i=0; i<orderedChildNodes.length;i++){
				 if(i==0)
					 emailMessage = emailMessage+orderedChildNodes[i];
				 else
					 emailMessage = emailMessage+", "+orderedChildNodes[i];
			 }
		 }
		 
		
	try {
		//Creare a MAP to populate the email template variables 
        Map myMap = new HashMap() ; 
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
            
         
        //Populate the MAP with the values submitted from the HTL front end
        myMap.put("topic.subject","AEM Author : Email notification from TestSeq Servlet"); 
        myMap.put("time",timeStamp); 
        myMap.put("post.message",emailMessage); 
        
        
       //Declare a MessageGateway service
       MessageGateway<HtmlEmail> messageGateway; 
                  
       //Specify the EMail template 
       String template ="/etc/notification/email/seqtest/sortnode/en.txt";
        
      Resource templateRsrc = request.getResourceResolver().getResource(template);
         
      MailTemplate mailTemplate = MailTemplate.create(templateRsrc.getPath(), templateRsrc.getResourceResolver().adaptTo(Session.class));
                
      	HtmlEmail email  = mailTemplate.getEmail(StrLookup.mapLookup(myMap), HtmlEmail.class);
		 email.addTo("sriramaraju.chekuri@gmail.com");
	     
	    
	      email.setFrom("sriramaraju.chekuri@gmail.com"); 
	    //Inject a MessageGateway Service and send the message
	      messageGateway = this.messageGatewayService.getGateway(HtmlEmail.class);
	      messageGateway.send((HtmlEmail) email);
	} catch (Exception e) {

		log.info("error",e);
		
		log.info("From Servlet Catch(emailMessage) = "+emailMessage);
		
		log.info("From Servlet Catch = "+e.getMessage());
	} 
                 
        
     
	}
}
