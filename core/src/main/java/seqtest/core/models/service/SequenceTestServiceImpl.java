package seqtest.core.models.service;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.day.cq.mcm.emailprovider.types.Email;



@Component(service=SequenceTestService.class)
@Designate(ocd=SeqTestDefinition.class)
public class SequenceTestServiceImpl implements SequenceTestService{
	
	public static final Logger LOGGER = LoggerFactory.getLogger(SequenceTestServiceImpl.class);

	private SeqTestDefinition config;
	
	private Session session;
	
	
	@Reference
    private SlingSettingsService settings;
	
	 @Activate
	    public void activate(SeqTestDefinition config) {
	        this.config = config;
	       
	    }
	 
	 /*
	  * Resource resolver objects can be accessed through the following ways
		ResourceResolverFactory
		Request processing servlet through the SlingHttpServletRequest.getResourceResolver() method.
		WCMUsePojo / WCMUse in case of Sightly using the getResourceResolver()
	  */
	 
	@Reference
	 ResourceResolverFactory resolverFactory;	   
	 
	 
	@Override
	public String[] ExecuteSortingOrder() {
		
		LOGGER.info("Starting to execute sorting....");
		
		Map<String,Object> param = new HashMap<String,Object>();
		//CREATE SYSTEM USER AT http://server:port/crx/explorer/index.jsp
		//User Created :seqtest
		//datawrite defined under service user mapper (components config)
		param.put(ResourceResolverFactory.SUBSERVICE, "datawrite");
		ResourceResolver resolver = null;
		String[] nameListOrdered = null;
		
		String path = config.getRootNodePath();
		String property = config.getProperty();
		String ASCorDSC= config.getOrder();
		LOGGER.info("path = "+path);
		LOGGER.info("property = "+property);
		LOGGER.info("ASCorDSC = "+ASCorDSC);
		
		try {
				resolver = resolverFactory.getServiceResourceResolver(param);
				session = resolver.adaptTo(Session.class);
				LOGGER.info("Session Created.");
				
				
				
				Node parentNode = session.getNode(path);
				LOGGER.info("node.getName() = "+parentNode.getName());
				
				LOGGER.info("has orderable child nodes ? : "+parentNode.getPrimaryNodeType().hasOrderableChildNodes());
				
				//Get Query Manager for this session.
				QueryManager queryManager = session.getWorkspace().getQueryManager();
				
				//Create an sqlStmt variable;
				String sqlStmt = null;
				
				sqlStmt="SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE(["+path+"]) and contains("+property+",'*')";
				
				LOGGER.info(sqlStmt);
				
				Query query = queryManager.createQuery(sqlStmt,"JCR-SQL2");
				
				//Execute the query and get the results ...
				QueryResult result = query.execute();
				            
				//Iterate over the nodes in the results ...
				
				NodeIterator nodeIter = result.getNodes();
				Map<String, Integer> unsortedNodeMap = new HashMap<String, Integer>();
				while ( nodeIter.hasNext() ) {
					Node node = nodeIter.nextNode();
					LOGGER.info(node.getName()+" : "+node.getProperty("SqNo").getString());
					
					unsortedNodeMap.put(node.getName(),Integer.parseInt(node.getProperty(property).getString()));
					
				}
				
				//Get UnOrdered Node name list in to String Array.
				String[] nameListUnOrdered = new String[unsortedNodeMap.size()];
				int l=0;
				for (Map.Entry<String, Integer> entry : unsortedNodeMap.entrySet()) {
					nameListUnOrdered[l] = entry.getKey();
				    l++;
				}
						
				//Sort the unordered list in ASC (Calling sortByValues())
				Map<String, Integer> sortedNodeMap = sortByValues(unsortedNodeMap);
				nameListOrdered = new String[sortedNodeMap.size()];
				
				//Get Ordered Node name list in to String Array.
				int i=0;
				for (Map.Entry<String, Integer> entry : sortedNodeMap.entrySet()) {
				    LOGGER.info(entry.getKey()+" : "+entry.getValue().toString());
				    nameListOrdered[i] = entry.getKey();
				    i++;
				}
				
				//If Config Value is DSC, resequence the nameListOrdered in descending order
				if(ASCorDSC.equalsIgnoreCase("DSC")){
					LOGGER.info("Resorting String to Descending Order");
					String temp; 
					int start = 0;
					int end = nameListOrdered.length-1;
					   
					while (start < end) 
					{ 
					    temp = nameListOrdered[start];  
					    nameListOrdered[start] = nameListOrdered[end]; 
					    nameListOrdered[end] = temp; 
					    start++; 
					    end--; 
					}  
					
					
					
				}
				
				String destRelPath = null;
				for(int k=nameListOrdered.length-1;k>-1;k--){			
					String srcRelPath = nameListOrdered[k];
					
	                if(k==nameListOrdered.length-1){
	                	destRelPath = nameListUnOrdered[1];
	                	parentNode.orderBefore(srcRelPath, destRelPath);
	                	destRelPath = srcRelPath;
	                	
	                }else{
	                	parentNode.orderBefore(srcRelPath, destRelPath);
	                	destRelPath = srcRelPath;
	                }
				}
				
				session.save();
				LOGGER.info("after reorder...");
				
				LOGGER.info("End to execute sorting....");
				
		}catch(Exception e){
			nameListOrdered = new String[2];
			nameListOrdered[0] = "FAILED";
			nameListOrdered[1] = e.getMessage();
			LOGGER.info(Integer.toString(nameListOrdered.length));
			LOGGER.info(nameListOrdered[0]);
			LOGGER.info(nameListOrdered[1]);
			LOGGER.info("from catch block : "+e.getMessage());
			e.printStackTrace();
		}finally {
			if(session.isLive()){
				LOGGER.info("inside finally");
				session.logout();
			}
		}
		
		
		return nameListOrdered;
	}
	
 

	 public static <K, V extends Comparable<V>> Map<K, V> sortByValues(final Map<K, V> map) {
		 LOGGER.info("starting sorting..");
	    Comparator<K> valueComparator = 
	             new Comparator<K>() {
	      public int compare(K k1, K k2) {
	        int compare = 
	              map.get(k1).compareTo(map.get(k2));
	        if (compare == 0) 
	          return 1;
	        else 
	          return compare;
	      }
	    };
	    
	    Map<K, V> sortedByValues = new TreeMap<K, V>(valueComparator);
	    sortedByValues.putAll(map);
	    LOGGER.info("end of sorting..");
	    
	    return sortedByValues;
	    	   
	 }
	 
	
	 
	
}
