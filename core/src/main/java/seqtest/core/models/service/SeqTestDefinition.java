package seqtest.core.models.service;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name="Sequence Test Service", description="This is a Sequence Test service")
public @interface SeqTestDefinition {

	@AttributeDefinition(name="name", description="Enter Project Path (root node)")
	String getRootNodePath();
	
	@AttributeDefinition(name="property", description="Enter Property name to be used for Sorting")
	String getProperty();
	
	@AttributeDefinition(name="order", description="Enter Sorting type (ASC or DSC)")
	String getOrder();
	
	@AttributeDefinition(name="email", description="Enter Email (Email to which notification has to be sent)")
	String getToEmail();
	
}