package seqtest.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.InjectionStrategy;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables=Resource.class)
public class SqNoModel {
	
	
	
	@ValueMapValue(name="SqNo", injectionStrategy=InjectionStrategy.OPTIONAL) 
	private String sqno;

	public String getSqNo() {
		return sqno;
	}
	
	
}
